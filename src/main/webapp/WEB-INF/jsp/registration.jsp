<!DOCTYPE html>
<html xmlns:th="https://thymeleaf.org"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Регистрация</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="/resources/css/style.css">
</head>

<body>
<div>
    <form th:method="POST" th:action="@{/registration}" th:object="${userForm}">
        <div>
            <label for="login">Nickname: </label>
            <input type="text" th:field="*{login}" id="login"/>
            <p style="color: red" th:if="${#fields.hasErrors('loginError')}" th:errors="*{loginError}">Error</p>
        </div>
        <div>
            <label for="age">Age: </label>
            <input type="text" th:field="*{age}" id="age"/>
            <p style="color: red" th:if="${#fields.hasErrors('age')}" th:errors="*{ageError}">Error</p>
        </div>
        <div>
            <label for="password">Password: </label>
            <input type="password" th:field="*{password}" id="password"/>
            <p style="color: red" th:if="${#fields.hasErrors('password')}" th:errors="*{passwordError}">Error</p>
        </div>
        <div>
            <label for="passwordConfirm">Password: </label>
            <input type="password" th:field="*{passwordConfirm}" id="passwordConfirm"/>
            <p style="color: red" th:if="${#fields.hasErrors('passwordConfirm')}" th:errors="*{passwordConfirm}">passwordError</p>
        </div>

        <input type="submit" name="Reg" value="Registration"/>
    </form>
    <a href="/">Главная</a>
</div>

</body>
</html>
