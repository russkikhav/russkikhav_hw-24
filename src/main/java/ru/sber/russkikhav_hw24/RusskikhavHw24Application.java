package ru.sber.russkikhav_hw24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("ru.sber.russkikhav_hw24.model")
@EnableJpaRepositories("ru.sber.russkikhav_hw24.repository")
public class RusskikhavHw24Application {

    public static void main(String[] args) {
        SpringApplication.run(RusskikhavHw24Application.class, args);
    }

}
