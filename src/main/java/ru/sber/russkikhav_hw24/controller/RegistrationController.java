package ru.sber.russkikhav_hw24.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.sber.russkikhav_hw24.model.User;
import ru.sber.russkikhav_hw24.service.UserService;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private UserService userService;

    private final static String REGISTER_VIEW = "registration";

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return REGISTER_VIEW;
    }

    @PostMapping
    public String postRegistration(@ModelAttribute("userForm") User userForm,
                                   BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return REGISTER_VIEW;
        }
        if (!userService.registration(userForm)) {
            model.addAttribute("loginError", "this login already exist");
            return REGISTER_VIEW;
        }
        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
            model.addAttribute("passwordError", "password and confirm password not equals");
            return REGISTER_VIEW;
        }
        return "redirect:/";
    }
}
