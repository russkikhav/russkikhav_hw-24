package ru.sber.russkikhav_hw24.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "t_wall")
@NoArgsConstructor
public class MsgWall {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    @ManyToOne
    private User nickname;

    private LocalDateTime date;

    @PrePersist
    void prePersist() {
        date = LocalDateTime.now();
    }

    public MsgWall(String text, User nickname) {
        this.text = text;
        this.nickname = nickname;
    }
}
