package ru.sber.russkikhav_hw24.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.russkikhav_hw24.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);
}
