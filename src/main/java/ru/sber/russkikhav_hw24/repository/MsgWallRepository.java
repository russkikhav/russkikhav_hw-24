package ru.sber.russkikhav_hw24.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.russkikhav_hw24.model.MsgWall;

public interface MsgWallRepository extends JpaRepository<MsgWall, Long> {
}
