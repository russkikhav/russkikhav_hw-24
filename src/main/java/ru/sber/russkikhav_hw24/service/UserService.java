package ru.sber.russkikhav_hw24.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.russkikhav_hw24.exception.UserNotFoundException;
import ru.sber.russkikhav_hw24.model.User;
import ru.sber.russkikhav_hw24.repository.UserRepository;

import java.util.List;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder cryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       BCryptPasswordEncoder cryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.cryptPasswordEncoder = cryptPasswordEncoder;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User not fount by id " + userId));
    }

    public boolean registration(User userRequest) {

        User savedUser = userRepository.findByLogin(userRequest.getLogin());
        if (savedUser != null) {
            return false;
        }
        savedUser = new User();
        System.out.println(userRequest);
        savedUser.setLogin(userRequest.getLogin());
        savedUser.setPassword(cryptPasswordEncoder.encode(userRequest.getPassword()));
        userRepository.save(savedUser);
        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
}
