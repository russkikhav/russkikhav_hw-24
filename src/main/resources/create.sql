create table t_user(
    id bigint AUTO_INCREMENT,
    login varchar(255),
    age integer,
    password varchar(255),
    primary key (id)
);

create table t_wall(
    id bigint AUTO_INCREMENT,
    text varchar(2000),
    nickname varchar(255),
    date DATE,
    primary key (id),

    foreign key (id) references t_user(id)
);

drop table t_user;
drop table t_wall;